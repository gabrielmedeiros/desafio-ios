//
//  MainViewController.h
//  Desafio-iOS
//
//  Created by Gabriel Medeiros on 03/09/14.
//  Copyright (c) 2014 Gabriel Medeiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController

@property (strong, nonatomic) NSMutableArray *historico;

@property (weak, nonatomic) IBOutlet UITextField *txtCEP;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progresso;

- (IBAction)consultar:(id)sender;
- (IBAction)historico:(id)sender;

@end

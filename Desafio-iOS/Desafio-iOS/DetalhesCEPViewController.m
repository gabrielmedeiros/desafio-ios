//
//  DetalhesCEPViewController.m
//  Desafio-iOS
//
//  Created by Gabriel Medeiros on 03/09/14.
//  Copyright (c) 2014 Gabriel Medeiros. All rights reserved.
//

#import "DetalhesCEPViewController.h"

@interface DetalhesCEPViewController ()

@end

@implementation DetalhesCEPViewController

@synthesize endereco;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"DETALHES";
    
    self.lblCEP.text = endereco.cep;
    self.lblBairro.text = endereco.bairro;
    self.lblCidade.text = endereco.cidade;
    self.lblEstado.text = endereco.estado;
    self.lblLogradouro.text = endereco.logradouro;
    self.lblTipoLogradouro.text = endereco.tipoDeLogradouro;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

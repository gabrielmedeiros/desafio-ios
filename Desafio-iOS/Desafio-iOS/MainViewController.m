//
//  MainViewController.m
//  Desafio-iOS
//
//  Created by Gabriel Medeiros on 03/09/14.
//  Copyright (c) 2014 Gabriel Medeiros. All rights reserved.
//

#import "MainViewController.h"

#import "AFNetworking.h"
#import "AKNumericFormatter.h"
#import <UITextField+AKNumericFormatter.h>
#import "Reachability.h"

#import "Endereco.h"
#import "HistoricoViewController.h"
#import "DetalhesCEPViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

@synthesize historico;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.progresso stopAnimating];
    
    self.txtCEP.numericFormatter = [AKNumericFormatter formatterWithMask:@"*****-***"
                                                    placeholderCharacter:'*'];
    
    self.title = @"INÍCIO";
    
    if (!historico) {
        historico = [[NSMutableArray alloc] init];
    }
    NSString *arquivo = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/historico"];
    historico = [NSKeyedUnarchiver unarchiveObjectWithFile:arquivo];
    if (!historico) {
        historico = [NSMutableArray arrayWithCapacity:0];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txtCEP resignFirstResponder];
}

- (IBAction)consultar:(id)sender {
    if (![self verificaConexao]) {
        UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:@"Desafio iOS"
                                                         message:@"Sem conexão."
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        [alerta show];
        return;
    }
    
    if ([self.txtCEP.text length] != 9) {
        UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:@"Desafio iOS"
                                                         message:@"Informe o CEP corretamente."
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        [alerta show];
        return;
    }
    
    [self.progresso startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    self.view.userInteractionEnabled = false;
    
    NSString *cepSemMascara = [self removeMascaraCEP:self.txtCEP.text];
    
    NSString *url = [NSString stringWithFormat:@"http://correiosapi.apphb.com/cep/%@", cepSemMascara];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *retorno) {
        NSError *error = nil;
        Endereco *endereco = [MTLJSONAdapter modelOfClass:Endereco.class fromJSONDictionary:retorno error:&error];
        endereco.cep = [self adicionaMascaraCEP:endereco.cep];
        
        [self.progresso stopAnimating];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = FALSE;
        self.view.userInteractionEnabled = true;
        
        [historico addObject:endereco];
        
        NSString *arquivo = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/historico"];
        [NSKeyedArchiver archiveRootObject:historico toFile:arquivo];
        
        DetalhesCEPViewController *tela = [[DetalhesCEPViewController alloc] init];
        tela.endereco = endereco;
        [self.navigationController pushViewController:tela animated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *mensagem = [[NSString alloc] init];
        switch ([operation.response statusCode]) {
            case 404: {
                mensagem = @"CEP não encontrado.";
                break;
            }
            case 400: {
                mensagem = @"Erro no servidor.";
                break;
            }
            default:
                break;
        }
        UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:@"Desafio iOS"
                                                         message:mensagem
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        [self.progresso stopAnimating];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = FALSE;
        self.view.userInteractionEnabled = true;
        [alerta show];
    }];
}

- (IBAction)historico:(id)sender {
    HistoricoViewController *tela = [[HistoricoViewController alloc] init];
    tela.historico = historico;
    [self.navigationController pushViewController:tela animated:YES];
}

-(BOOL)verificaConexao
{
    BOOL retorno;
    
    Reachability *reachability = [Reachability reachabilityWithHostName:@"google.com"];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable) {
        retorno  = FALSE;
    } else if (remoteHostStatus == ReachableViaWWAN) {
        retorno = TRUE;
    } else if (remoteHostStatus == ReachableViaWiFi) {
        retorno = TRUE;
    }
    
    return retorno;
}

-(NSString*)removeMascaraCEP:(NSString*)cep
{
    NSArray *numeros = [cep componentsSeparatedByString:@"-"];
    return [NSString stringWithFormat:@"%@%@", [numeros objectAtIndex:0], [numeros objectAtIndex:1]];
}

-(NSString*)adicionaMascaraCEP:(NSString*)cep
{
    return [AKNumericFormatter formatString:cep
                                  usingMask:@"*****-***"
                       placeholderCharacter:'*'];
}

@end

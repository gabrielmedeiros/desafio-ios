//
//  Endereco.h
//  Desafio-iOS
//
//  Created by Gabriel Medeiros on 03/09/14.
//  Copyright (c) 2014 Gabriel Medeiros. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface Endereco : MTLModel <MTLJSONSerializing>

@property (nonatomic) NSString* cep;
@property (nonatomic, copy, readonly) NSString* tipoDeLogradouro;
@property (nonatomic, copy, readonly) NSString* logradouro;
@property (nonatomic, copy, readonly) NSString* bairro;
@property (nonatomic, copy, readonly) NSString* cidade;
@property (nonatomic, copy, readonly) NSString* estado;

@end

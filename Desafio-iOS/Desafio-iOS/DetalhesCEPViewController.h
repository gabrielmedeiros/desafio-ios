//
//  DetalhesCEPViewController.h
//  Desafio-iOS
//
//  Created by Gabriel Medeiros on 03/09/14.
//  Copyright (c) 2014 Gabriel Medeiros. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Endereco.h"

@interface DetalhesCEPViewController : UIViewController

@property Endereco *endereco;

@property (weak, nonatomic) IBOutlet UILabel *lblCEP;
@property (weak, nonatomic) IBOutlet UILabel *lblTipoLogradouro;
@property (weak, nonatomic) IBOutlet UILabel *lblLogradouro;
@property (weak, nonatomic) IBOutlet UILabel *lblBairro;
@property (weak, nonatomic) IBOutlet UILabel *lblCidade;
@property (weak, nonatomic) IBOutlet UILabel *lblEstado;

@end

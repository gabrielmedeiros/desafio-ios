//
//  HistoricoViewController.h
//  Desafio-iOS
//
//  Created by Gabriel Medeiros on 03/09/14.
//  Copyright (c) 2014 Gabriel Medeiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoricoViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *historico;

@property (weak, nonatomic) IBOutlet UITableView *tbvHistorico;

@end

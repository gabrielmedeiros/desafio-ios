//
//  Endereco.m
//  Desafio-iOS
//
//  Created by Gabriel Medeiros on 03/09/14.
//  Copyright (c) 2014 Gabriel Medeiros. All rights reserved.
//

#import "Endereco.h"

@implementation Endereco

+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
        @"cep": @"cep",
        @"tipoDeLogradouro": @"tipoDeLogradouro",
        @"logradouro": @"logradouro",
        @"bairro": @"bairro",
        @"cidade":@"cidade",
        @"estado":@"estado"
    };
}

@end

//
//  HistoricoViewController.m
//  Desafio-iOS
//
//  Created by Gabriel Medeiros on 03/09/14.
//  Copyright (c) 2014 Gabriel Medeiros. All rights reserved.
//

#import "HistoricoViewController.h"

#import "Endereco.h"
#import "DetalhesCEPViewController.h"

@interface HistoricoViewController ()

@end

@implementation HistoricoViewController

@synthesize historico;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"HISTÓRICO";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [historico count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"Cell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	NSInteger linha = indexPath.row;
    
	if (nil == cell)
	{
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
	}
	
    cell.textLabel.font = [UIFont boldSystemFontOfSize:17];
    
    Endereco *endereco = [historico objectAtIndex:linha];
    
    cell.textLabel.text = [NSString stringWithFormat:@"CEP: %@", endereco.cep];
    
	return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetalhesCEPViewController *tela = [[DetalhesCEPViewController alloc] init];
    tela.endereco = [historico objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:tela animated:YES];
}

@end
